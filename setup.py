from setuptools import setup, find_packages
setup(
    name='sphinx_easypython_theme',
    version='0.10',
    author='Julien Robert',
    author_email='julien.robert@univ-orleans.fr',
    packages=find_packages(),
    install_requires=['sphinx-bootstrap-theme'],
    dependency_links=[],
        entry_points = {
            'sphinx.html_themes': [
                'sphinx_easypython_theme = sphinx_easypython_theme',
                ]
            },
        )



