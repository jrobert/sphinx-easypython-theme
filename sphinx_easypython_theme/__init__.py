from os import path
from sphinx_bootstrap_theme import get_html_theme_path as get_html_theme_path_bt

def setup(app):
    #setup_bt(app)
    app.add_html_theme('sphinx_easypython_theme', path.abspath(path.dirname(__file__)))

def get_html_theme_path():
    cur_dir = path.abspath(path.dirname(__file__))
    return  [cur_dir] + get_html_theme_path_bt() 

